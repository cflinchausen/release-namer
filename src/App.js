import React, { Component } from 'react';
import DatePicker from 'react-date-picker';
import namedays from './namedays';
import './App.css';

function Names(props) {
  console.log(props);
  const names = props.names.map(name => {
    return <li key={name}>{name}</li>;
  });

  return <ul>{names}</ul>;
}

class App extends Component {
  constructor() {
    super();

    const today = new Date();
    this.state = {
      selectedDay: today
    };

    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    this.setState({
      selectedDay: date
    });
  }

  getNames() {
    const { selectedDay } = this.state;

    if (!selectedDay) return;
    const day =
      selectedDay.getDate() < 10
        ? `0${selectedDay.getDate()}`
        : selectedDay.getDate();

    const month = selectedDay.getMonth() + 1;
    const key = `${day}.${month < 10 ? `0${month}` : month}`;

    return namedays.filter(nameDayPair => {
      return nameDayPair.day === key;
    })[0];
  }

  render() {
    const names = this.getNames();

    return (
      <div className="App">
        <main className="App-main">
          <h1>Velg dato for å finne release navn</h1>
          <DatePicker
            value={this.state.selectedDay}
            onChange={this.onDateChange}
          />
          {names && <Names names={names.names} />}
        </main>
      </div>
    );
  }
}

export default App;
