const namedays = [
    {
      "day": "01.01",
      "names": [
        "(ingen",
        "navnedag)"
      ]
    },
    {
      "day": "02.01",
      "names": [
        "Dagfinn",
        "Dagfrid"
      ]
    },
    {
      "day": "03.01",
      "names": [
        "Alfred",
        "Alf"
      ]
    },
    {
      "day": "04.01",
      "names": [
        "Roar",
        "Roger"
      ]
    },
    {
      "day": "05.01",
      "names": [
        "Hanna",
        "Hanne"
      ]
    },
    {
      "day": "06.01",
      "names": [
        "Aslaug",
        "Åslaug"
      ]
    },
    {
      "day": "07.01",
      "names": [
        "Eldbjørg",
        "Knut"
      ]
    },
    {
      "day": "08.01",
      "names": [
        "Torfinn",
        "Turid"
      ]
    },
    {
      "day": "09.01",
      "names": [
        "Gunnar",
        "Gunn"
      ]
    },
    {
      "day": "10.01",
      "names": [
        "Sigmund",
        "Sigrun"
      ]
    },
    {
      "day": "11.01",
      "names": [
        "Børge",
        "Børre"
      ]
    },
    {
      "day": "12.01",
      "names": [
        "Reinhard",
        "Reinert"
      ]
    },
    {
      "day": "13.01",
      "names": [
        "Gisle",
        "Gislaug"
      ]
    },
    {
      "day": "14.01",
      "names": [
        "Herbjørn",
        "Herbjørg"
      ]
    },
    {
      "day": "15.01",
      "names": [
        "Laurits",
        "Laura"
      ]
    },
    {
      "day": "16.01",
      "names": [
        "Hjalmar",
        "Hilmar"
      ]
    },
    {
      "day": "17.01",
      "names": [
        "Anton",
        "Tønnes",
        "Tony"
      ]
    },
    {
      "day": "18.01",
      "names": [
        "Hildur",
        "Hild"
      ]
    },
    {
      "day": "19.01",
      "names": [
        "Marius",
        "Margunn"
      ]
    },
    {
      "day": "20.01",
      "names": [
        "Fabian",
        "Sebastian",
        "Bastian"
      ]
    },
    {
      "day": "21.01",
      "names": [
        "Agnes",
        "Agnete"
      ]
    },
    {
      "day": "22.01",
      "names": [
        "Ivan",
        "Vanja"
      ]
    },
    {
      "day": "23.01",
      "names": [
        "Emil",
        "Emilie",
        "Emma"
      ]
    },
    {
      "day": "24.01",
      "names": [
        "Joar",
        "Jarle",
        "Jarl"
      ]
    },
    {
      "day": "25.01",
      "names": [
        "Paul",
        "Pål"
      ]
    },
    {
      "day": "26.01",
      "names": [
        "Øystein",
        "Esten"
      ]
    },
    {
      "day": "27.01",
      "names": [
        "Gaute",
        "Gurly",
        "Gry"
      ]
    },
    {
      "day": "28.01",
      "names": [
        "Karl",
        "Karoline"
      ]
    },
    {
      "day": "29.01",
      "names": [
        "Herdis",
        "Hermod",
        "Hermann"
      ]
    },
    {
      "day": "30.01",
      "names": [
        "Gunnhild",
        "Gunda"
      ]
    },
    {
      "day": "31.01",
      "names": [
        "Idun",
        "Ivar"
      ]
    },
    {
      "day": "01.02",
      "names": [
        "Birte",
        "Bjarte"
      ]
    },
    {
      "day": "02.02",
      "names": [
        "Jomar",
        "Jostein"
      ]
    },
    {
      "day": "03.02",
      "names": [
        "Ansgar",
        "Asgeir"
      ]
    },
    {
      "day": "04.02",
      "names": [
        "Veronika",
        "Vera"
      ]
    },
    {
      "day": "05.02",
      "names": [
        "Agate",
        "Ågot"
      ]
    },
    {
      "day": "06.02",
      "names": [
        "Dortea",
        "Dorte"
      ]
    },
    {
      "day": "07.02",
      "names": [
        "Rikard",
        "Rigmor",
        "Riborg"
      ]
    },
    {
      "day": "08.02",
      "names": [
        "Åshild",
        "Åsne"
      ]
    },
    {
      "day": "09.02",
      "names": [
        "Lone",
        "Leikny"
      ]
    },
    {
      "day": "10.02",
      "names": [
        "Ingfrid",
        "Ingrid"
      ]
    },
    {
      "day": "11.02",
      "names": [
        "Ingve",
        "Yngve"
      ]
    },
    {
      "day": "12.02",
      "names": [
        "Randulf",
        "Randi",
        "Ronja"
      ]
    },
    {
      "day": "13.02",
      "names": [
        "Svanhild",
        "Svanlaug"
      ]
    },
    {
      "day": "14.02",
      "names": [
        "Hjørdis",
        "Jardar"
      ]
    },
    {
      "day": "15.02",
      "names": [
        "Sigfred",
        "Sigbjørn"
      ]
    },
    {
      "day": "16.02",
      "names": [
        "Julian",
        "Juliane",
        "Jill"
      ]
    },
    {
      "day": "17.02",
      "names": [
        "Aleksandra",
        "Sandra",
        "Sondre"
      ]
    },
    {
      "day": "18.02",
      "names": [
        "Frøydis",
        "Frode"
      ]
    },
    {
      "day": "19.02",
      "names": [
        "Ella",
        "Elna"
      ]
    },
    {
      "day": "20.02",
      "names": [
        "Halldis",
        "Halldor"
      ]
    },
    {
      "day": "21.02",
      "names": [
        "Samuel",
        "Selma",
        "Celine"
      ]
    },
    {
      "day": "22.02",
      "names": [
        "Tina",
        "Tine",
        "Tim"
      ]
    },
    {
      "day": "23.02",
      "names": [
        "Torstein",
        "Torunn"
      ]
    },
    {
      "day": "24.02",
      "names": [
        "Mattias",
        "Mattis",
        "Mats"
      ]
    },
    {
      "day": "25.02",
      "names": [
        "Viktor",
        "Viktoria"
      ]
    },
    {
      "day": "26.02",
      "names": [
        "Inger",
        "Ingjerd"
      ]
    },
    {
      "day": "27.02",
      "names": [
        "Laila",
        "Lill"
      ]
    },
    {
      "day": "28.02",
      "names": [
        "Marina",
        "Maren"
      ]
    },
    {
      "day": "01.03",
      "names": [
        "Audny",
        "Audun"
      ]
    },
    {
      "day": "02.03",
      "names": [
        "Erna",
        "Ernst"
      ]
    },
    {
      "day": "03.03",
      "names": [
        "Gunnbjørg",
        "Gunnveig"
      ]
    },
    {
      "day": "04.03",
      "names": [
        "Ada",
        "Adrian"
      ]
    },
    {
      "day": "05.03",
      "names": [
        "Patrick",
        "Patricia"
      ]
    },
    {
      "day": "06.03",
      "names": [
        "Annfrid",
        "Andor"
      ]
    },
    {
      "day": "07.03",
      "names": [
        "Arild",
        "Are"
      ]
    },
    {
      "day": "08.03",
      "names": [
        "Beate",
        "Betty",
        "Bettina"
      ]
    },
    {
      "day": "09.03",
      "names": [
        "Sverre",
        "Sindre"
      ]
    },
    {
      "day": "10.03",
      "names": [
        "Edel",
        "Edle"
      ]
    },
    {
      "day": "11.03",
      "names": [
        "Edvin",
        "Tale"
      ]
    },
    {
      "day": "12.03",
      "names": [
        "Gregor",
        "Gro"
      ]
    },
    {
      "day": "13.03",
      "names": [
        "Greta",
        "Grete"
      ]
    },
    {
      "day": "14.03",
      "names": [
        "Mathilde",
        "Mette"
      ]
    },
    {
      "day": "15.03",
      "names": [
        "Christel",
        "Christer",
        "Chris"
      ]
    },
    {
      "day": "16.03",
      "names": [
        "Gudmund",
        "Gudny"
      ]
    },
    {
      "day": "17.03",
      "names": [
        "Gjertrud",
        "Trude"
      ]
    },
    {
      "day": "18.03",
      "names": [
        "Aleksander",
        "Sander",
        "Edvard"
      ]
    },
    {
      "day": "19.03",
      "names": [
        "Josef",
        "Josefine"
      ]
    },
    {
      "day": "20.03",
      "names": [
        "Joakim",
        "Kim"
      ]
    },
    {
      "day": "21.03",
      "names": [
        "Bendik",
        "Bengt",
        "Bent"
      ]
    },
    {
      "day": "22.03",
      "names": [
        "Paula",
        "Pauline"
      ]
    },
    {
      "day": "23.03",
      "names": [
        "Gerda",
        "Gerd"
      ]
    },
    {
      "day": "24.03",
      "names": [
        "Ulrikke",
        "Rikke"
      ]
    },
    {
      "day": "25.03",
      "names": [
        "Maria",
        "Marie",
        "Mari"
      ]
    },
    {
      "day": "26.03",
      "names": [
        "Gabriel",
        "Glenn"
      ]
    },
    {
      "day": "27.03",
      "names": [
        "Rudolf",
        "Rudi"
      ]
    },
    {
      "day": "28.03",
      "names": [
        "Åsta",
        "Åste"
      ]
    },
    {
      "day": "29.03",
      "names": [
        "Jonas",
        "Jonatan"
      ]
    },
    {
      "day": "30.03",
      "names": [
        "Holger",
        "Olga"
      ]
    },
    {
      "day": "31.03",
      "names": [
        "Vebjørn",
        "Vegard"
      ]
    },
    {
      "day": "01.04",
      "names": [
        "Aron",
        "Arve",
        "Arvid"
      ]
    },
    {
      "day": "02.04",
      "names": [
        "Sigvard",
        "Sivert"
      ]
    },
    {
      "day": "03.04",
      "names": [
        "Gunnvald",
        "Gunnvor"
      ]
    },
    {
      "day": "04.04",
      "names": [
        "Nanna",
        "Nancy",
        "Nina"
      ]
    },
    {
      "day": "05.04",
      "names": [
        "Irene",
        "Eirin",
        "Eril"
      ]
    },
    {
      "day": "06.04",
      "names": [
        "Åsmund",
        "Asmund"
      ]
    },
    {
      "day": "07.04",
      "names": [
        "Oddveig",
        "Oddvin"
      ]
    },
    {
      "day": "08.04",
      "names": [
        "Asle",
        "Atle"
      ]
    },
    {
      "day": "09.04",
      "names": [
        "Rannveig",
        "Rønnaug"
      ]
    },
    {
      "day": "10.04",
      "names": [
        "Ingvald",
        "Ingveig"
      ]
    },
    {
      "day": "11.04",
      "names": [
        "Ylva",
        "Ulf"
      ]
    },
    {
      "day": "12.04",
      "names": [
        "Julius",
        "Julie"
      ]
    },
    {
      "day": "13.04",
      "names": [
        "Asta",
        "Astrid"
      ]
    },
    {
      "day": "14.04",
      "names": [
        "Ellinor",
        "Nora"
      ]
    },
    {
      "day": "15.04",
      "names": [
        "Oda",
        "Odin",
        "Odd"
      ]
    },
    {
      "day": "16.04",
      "names": [
        "Magnus",
        "Mons"
      ]
    },
    {
      "day": "17.04",
      "names": [
        "Elise",
        "Else",
        "Elsa"
      ]
    },
    {
      "day": "18.04",
      "names": [
        "Eilen",
        "Eilert"
      ]
    },
    {
      "day": "19.04",
      "names": [
        "Arnfinn",
        "Arnstein"
      ]
    },
    {
      "day": "20.04",
      "names": [
        "Kjellaug",
        "Kjellrun"
      ]
    },
    {
      "day": "21.04",
      "names": [
        "Jeanette",
        "Jannike"
      ]
    },
    {
      "day": "22.04",
      "names": [
        "Oddgeir",
        "Oddny"
      ]
    },
    {
      "day": "23.04",
      "names": [
        "Georg",
        "Jørgen",
        "Jørg"
      ]
    },
    {
      "day": "24.04",
      "names": [
        "Albert",
        "Olaug"
      ]
    },
    {
      "day": "25.04",
      "names": [
        "Markus",
        "Mark"
      ]
    },
    {
      "day": "26.04",
      "names": [
        "Terese",
        "Tea"
      ]
    },
    {
      "day": "27.04",
      "names": [
        "Charles",
        "Charlotte",
        "Lotte"
      ]
    },
    {
      "day": "28.04",
      "names": [
        "Vivi",
        "Vivian"
      ]
    },
    {
      "day": "29.04",
      "names": [
        "Toralf",
        "Torolf"
      ]
    },
    {
      "day": "30.04",
      "names": [
        "Gina",
        "Gitte"
      ]
    },
    {
      "day": "01.05",
      "names": [
        "Filip",
        "Valborg"
      ]
    },
    {
      "day": "02.05",
      "names": [
        "Åsa",
        "Åse"
      ]
    },
    {
      "day": "03.05",
      "names": [
        "Gjermund",
        "Gøril"
      ]
    },
    {
      "day": "04.05",
      "names": [
        "Monika",
        "Mona"
      ]
    },
    {
      "day": "05.05",
      "names": [
        "Gudbrand",
        "Gullborg"
      ]
    },
    {
      "day": "06.05",
      "names": [
        "Guri",
        "Gyri"
      ]
    },
    {
      "day": "07.05",
      "names": [
        "Maia",
        "Mai",
        "Maiken"
      ]
    },
    {
      "day": "08.05",
      "names": [
        "Åge",
        "Åke"
      ]
    },
    {
      "day": "09.05",
      "names": [
        "Kasper",
        "Jesper"
      ]
    },
    {
      "day": "10.05",
      "names": [
        "Asbjørg",
        "Asbjørn",
        "Espen"
      ]
    },
    {
      "day": "11.05",
      "names": [
        "Magda",
        "Malvin"
      ]
    },
    {
      "day": "12.05",
      "names": [
        "Normann",
        "Norvald"
      ]
    },
    {
      "day": "13.05",
      "names": [
        "Linda",
        "Line",
        "Linn"
      ]
    },
    {
      "day": "14.05",
      "names": [
        "Kristian",
        "Kristen",
        "Karsten"
      ]
    },
    {
      "day": "15.05",
      "names": [
        "Hallvard",
        "Hallvor"
      ]
    },
    {
      "day": "16.05",
      "names": [
        "Sara",
        "Siren"
      ]
    },
    {
      "day": "17.05",
      "names": [
        "Harald",
        "Ragnhild"
      ]
    },
    {
      "day": "18.05",
      "names": [
        "Eirik",
        "Erik",
        "Erika"
      ]
    },
    {
      "day": "19.05",
      "names": [
        "Torjus",
        "Torje",
        "Truls"
      ]
    },
    {
      "day": "20.05",
      "names": [
        "Bjørnar",
        "Bjørnhild"
      ]
    },
    {
      "day": "21.05",
      "names": [
        "Helene",
        "Ellen",
        "Eli"
      ]
    },
    {
      "day": "22.05",
      "names": [
        "Henning",
        "Henny"
      ]
    },
    {
      "day": "23.05",
      "names": [
        "Oddleif",
        "Oddlaug"
      ]
    },
    {
      "day": "24.05",
      "names": [
        "Ester",
        "Iris"
      ]
    },
    {
      "day": "25.05",
      "names": [
        "Ragna",
        "Ragnar"
      ]
    },
    {
      "day": "26.05",
      "names": [
        "Annbjørg",
        "Annlaug"
      ]
    },
    {
      "day": "27.05",
      "names": [
        "Katinka",
        "Cato"
      ]
    },
    {
      "day": "28.05",
      "names": [
        "Vilhelm",
        "William",
        "Willy"
      ]
    },
    {
      "day": "29.05",
      "names": [
        "Magnar",
        "Magnhild"
      ]
    },
    {
      "day": "30.05",
      "names": [
        "Gard",
        "Geir"
      ]
    },
    {
      "day": "31.05",
      "names": [
        "Pernille",
        "Preben"
      ]
    },
    {
      "day": "01.06",
      "names": [
        "June",
        "Juni"
      ]
    },
    {
      "day": "02.06",
      "names": [
        "Runa",
        "Runar",
        "Rune"
      ]
    },
    {
      "day": "03.06",
      "names": [
        "Rasmus",
        "Rakel"
      ]
    },
    {
      "day": "04.06",
      "names": [
        "Heidi",
        "Heid"
      ]
    },
    {
      "day": "05.06",
      "names": [
        "Torbjørg",
        "Torbjørn",
        "Torben"
      ]
    },
    {
      "day": "06.06",
      "names": [
        "Gustav",
        "Gyda"
      ]
    },
    {
      "day": "07.06",
      "names": [
        "Robert",
        "Robin"
      ]
    },
    {
      "day": "08.06",
      "names": [
        "Renate",
        "René"
      ]
    },
    {
      "day": "09.06",
      "names": [
        "Kolbein",
        "Kolbjørn"
      ]
    },
    {
      "day": "10.06",
      "names": [
        "Ingolf",
        "Ingunn"
      ]
    },
    {
      "day": "11.06",
      "names": [
        "Borgar",
        "Bjørge",
        "Bjørg"
      ]
    },
    {
      "day": "12.06",
      "names": [
        "Sigfrid",
        "Sigrid",
        "Siri"
      ]
    },
    {
      "day": "13.06",
      "names": [
        "Tone",
        "Tonje",
        "Tanja"
      ]
    },
    {
      "day": "14.06",
      "names": [
        "Erlend",
        "Erland"
      ]
    },
    {
      "day": "15.06",
      "names": [
        "Vigdis",
        "Viggo"
      ]
    },
    {
      "day": "16.06",
      "names": [
        "Torhild",
        "Toril",
        "Tiril"
      ]
    },
    {
      "day": "17.06",
      "names": [
        "Botolf",
        "Bodil"
      ]
    },
    {
      "day": "18.06",
      "names": [
        "Bjarne",
        "Bjørn"
      ]
    },
    {
      "day": "19.06",
      "names": [
        "Erling",
        "Elling"
      ]
    },
    {
      "day": "20.06",
      "names": [
        "Salve",
        "Sølve",
        "Sølvi"
      ]
    },
    {
      "day": "21.06",
      "names": [
        "Agnar",
        "Annar"
      ]
    },
    {
      "day": "22.06",
      "names": [
        "Håkon",
        "Maud"
      ]
    },
    {
      "day": "23.06",
      "names": [
        "Elfrid",
        "Eldrid"
      ]
    },
    {
      "day": "24.06",
      "names": [
        "Johannes",
        "Jon",
        "Hans"
      ]
    },
    {
      "day": "25.06",
      "names": [
        "Jørund",
        "Jorunn"
      ]
    },
    {
      "day": "26.06",
      "names": [
        "Jenny",
        "Jonny"
      ]
    },
    {
      "day": "27.06",
      "names": [
        "Aina",
        "Ina",
        "Ine"
      ]
    },
    {
      "day": "28.06",
      "names": [
        "Lea",
        "Leo",
        "Leon"
      ]
    },
    {
      "day": "29.06",
      "names": [
        "Peter",
        "Petter",
        "Per"
      ]
    },
    {
      "day": "30.06",
      "names": [
        "Solbjørg",
        "Solgunn"
      ]
    },
    {
      "day": "01.07",
      "names": [
        "Ask",
        "Embla"
      ]
    },
    {
      "day": "02.07",
      "names": [
        "Kjartan",
        "Kjellfrid"
      ]
    },
    {
      "day": "03.07",
      "names": [
        "Andrea",
        "Andrine",
        "André"
      ]
    },
    {
      "day": "04.07",
      "names": [
        "Ulrik",
        "Ulla"
      ]
    },
    {
      "day": "05.07",
      "names": [
        "Mirjam",
        "Mina"
      ]
    },
    {
      "day": "06.07",
      "names": [
        "Torgrim",
        "Torgunn"
      ]
    },
    {
      "day": "07.07",
      "names": [
        "Håvard",
        "Hulda"
      ]
    },
    {
      "day": "08.07",
      "names": [
        "Sunniva",
        "Synnøve",
        "Synne"
      ]
    },
    {
      "day": "09.07",
      "names": [
        "Gøran",
        "Jøran",
        "Ørjan"
      ]
    },
    {
      "day": "10.07",
      "names": [
        "Anita",
        "Anja"
      ]
    },
    {
      "day": "11.07",
      "names": [
        "Kjetil",
        "Kjell"
      ]
    },
    {
      "day": "12.07",
      "names": [
        "Elias",
        "Eldar"
      ]
    },
    {
      "day": "13.07",
      "names": [
        "Mildrid",
        "Melissa",
        "Mia"
      ]
    },
    {
      "day": "14.07",
      "names": [
        "Solfrid",
        "Solrun"
      ]
    },
    {
      "day": "15.07",
      "names": [
        "Oddmund",
        "Oddrun"
      ]
    },
    {
      "day": "16.07",
      "names": [
        "Susanne",
        "Sanna"
      ]
    },
    {
      "day": "17.07",
      "names": [
        "Guttorm",
        "Gorm"
      ]
    },
    {
      "day": "18.07",
      "names": [
        "Arnulf",
        "Ørnulf"
      ]
    },
    {
      "day": "19.07",
      "names": [
        "Gerhard",
        "Gjert"
      ]
    },
    {
      "day": "20.07",
      "names": [
        "Margareta",
        "Margit",
        "Marit"
      ]
    },
    {
      "day": "21.07",
      "names": [
        "Johanne",
        "Janne",
        "Jane"
      ]
    },
    {
      "day": "22.07",
      "names": [
        "Malene",
        "Malin",
        "Mali"
      ]
    },
    {
      "day": "23.07",
      "names": [
        "Brita",
        "Brit",
        "Britt"
      ]
    },
    {
      "day": "24.07",
      "names": [
        "Kristine",
        "Kristin",
        "Kristi"
      ]
    },
    {
      "day": "25.07",
      "names": [
        "Jakob",
        "Jack",
        "Jim"
      ]
    },
    {
      "day": "26.07",
      "names": [
        "Anna",
        "Anne",
        "Ane"
      ]
    },
    {
      "day": "27.07",
      "names": [
        "Marita",
        "Rita"
      ]
    },
    {
      "day": "28.07",
      "names": [
        "Reidar",
        "Reidun"
      ]
    },
    {
      "day": "29.07",
      "names": [
        "Olav",
        "Ola",
        "Ole"
      ]
    },
    {
      "day": "30.07",
      "names": [
        "Aurora",
        "Audhild",
        "Aud"
      ]
    },
    {
      "day": "31.07",
      "names": [
        "Elin",
        "Eline"
      ]
    },
    {
      "day": "01.08",
      "names": [
        "Peder",
        "Petra"
      ]
    },
    {
      "day": "02.08",
      "names": [
        "Karen",
        "Karin"
      ]
    },
    {
      "day": "03.08",
      "names": [
        "Oline",
        "Oliver",
        "Olve"
      ]
    },
    {
      "day": "04.08",
      "names": [
        "Arnhild",
        "Arna",
        "Arnt"
      ]
    },
    {
      "day": "05.08",
      "names": [
        "Osvald",
        "Oskar"
      ]
    },
    {
      "day": "06.08",
      "names": [
        "Gunnlaug",
        "Gunnleiv"
      ]
    },
    {
      "day": "07.08",
      "names": [
        "Didrik",
        "Doris"
      ]
    },
    {
      "day": "08.08",
      "names": [
        "Evy",
        "Yvonne"
      ]
    },
    {
      "day": "09.08",
      "names": [
        "Ronald",
        "Ronny"
      ]
    },
    {
      "day": "10.08",
      "names": [
        "Lorents",
        "Lars",
        "Lasse"
      ]
    },
    {
      "day": "11.08",
      "names": [
        "Torvald",
        "Tarald"
      ]
    },
    {
      "day": "12.08",
      "names": [
        "Klara",
        "Camilla"
      ]
    },
    {
      "day": "13.08",
      "names": [
        "Anny",
        "Anine",
        "Ann"
      ]
    },
    {
      "day": "14.08",
      "names": [
        "Hallgeir",
        "Hallgjerd"
      ]
    },
    {
      "day": "15.08",
      "names": [
        "Margot",
        "Mary",
        "Marielle"
      ]
    },
    {
      "day": "16.08",
      "names": [
        "Brynjulf",
        "Brynhild"
      ]
    },
    {
      "day": "17.08",
      "names": [
        "Verner",
        "Wenche"
      ]
    },
    {
      "day": "18.08",
      "names": [
        "Tormod",
        "Torodd"
      ]
    },
    {
      "day": "19.08",
      "names": [
        "Sigvald",
        "Sigve"
      ]
    },
    {
      "day": "20.08",
      "names": [
        "Bernhard",
        "Bernt"
      ]
    },
    {
      "day": "21.08",
      "names": [
        "Ragnvald",
        "Ragni"
      ]
    },
    {
      "day": "22.08",
      "names": [
        "Harriet",
        "Harry"
      ]
    },
    {
      "day": "23.08",
      "names": [
        "Signe",
        "Signy"
      ]
    },
    {
      "day": "24.08",
      "names": [
        "Belinda",
        "Bertil"
      ]
    },
    {
      "day": "25.08",
      "names": [
        "Ludvig",
        "Lovise",
        "Louise"
      ]
    },
    {
      "day": "26.08",
      "names": [
        "Øyvind",
        "Eivind",
        "Even"
      ]
    },
    {
      "day": "27.08",
      "names": [
        "Roald",
        "Rolf"
      ]
    },
    {
      "day": "28.08",
      "names": [
        "Artur",
        "August"
      ]
    },
    {
      "day": "29.08",
      "names": [
        "Johan",
        "Jone",
        "Jo"
      ]
    },
    {
      "day": "30.08",
      "names": [
        "Benjamin",
        "Ben"
      ]
    },
    {
      "day": "31.08",
      "names": [
        "Berta",
        "Berte"
      ]
    },
    {
      "day": "01.09",
      "names": [
        "Solveig",
        "Solvår"
      ]
    },
    {
      "day": "02.09",
      "names": [
        "Lisa",
        "Lise",
        "Liss"
      ]
    },
    {
      "day": "03.09",
      "names": [
        "Alise",
        "Alvhild",
        "Vilde"
      ]
    },
    {
      "day": "04.09",
      "names": [
        "Ida",
        "Idar"
      ]
    },
    {
      "day": "05.09",
      "names": [
        "Brede",
        "Brian",
        "Njål"
      ]
    },
    {
      "day": "06.09",
      "names": [
        "Sollaug",
        "Siril",
        "Siv"
      ]
    },
    {
      "day": "07.09",
      "names": [
        "Regine",
        "Rose"
      ]
    },
    {
      "day": "08.09",
      "names": [
        "Amalie",
        "Alma",
        "Allan"
      ]
    },
    {
      "day": "09.09",
      "names": [
        "Trygve",
        "Tyra",
        "Trym"
      ]
    },
    {
      "day": "10.09",
      "names": [
        "Tord",
        "Tor"
      ]
    },
    {
      "day": "11.09",
      "names": [
        "Dagny",
        "Dag"
      ]
    },
    {
      "day": "12.09",
      "names": [
        "Jofrid",
        "Jorid"
      ]
    },
    {
      "day": "13.09",
      "names": [
        "Stian",
        "Stig"
      ]
    },
    {
      "day": "14.09",
      "names": [
        "Ingebjørg",
        "Ingeborg"
      ]
    },
    {
      "day": "15.09",
      "names": [
        "Aslak",
        "Eskil"
      ]
    },
    {
      "day": "16.09",
      "names": [
        "Lillian",
        "Lilly"
      ]
    },
    {
      "day": "17.09",
      "names": [
        "Hildebjørg",
        "Hildegunn"
      ]
    },
    {
      "day": "18.09",
      "names": [
        "Henriette",
        "Henry"
      ]
    },
    {
      "day": "19.09",
      "names": [
        "Konstanse",
        "Connie"
      ]
    },
    {
      "day": "20.09",
      "names": [
        "Tobias",
        "Tage"
      ]
    },
    {
      "day": "21.09",
      "names": [
        "Trine",
        "Trond"
      ]
    },
    {
      "day": "22.09",
      "names": [
        "Kyrre",
        "Kåre"
      ]
    },
    {
      "day": "23.09",
      "names": [
        "Snorre",
        "Snefrid"
      ]
    },
    {
      "day": "24.09",
      "names": [
        "Jan",
        "Jens"
      ]
    },
    {
      "day": "25.09",
      "names": [
        "Ingvar",
        "Yngvar"
      ]
    },
    {
      "day": "26.09",
      "names": [
        "Einar",
        "Endre"
      ]
    },
    {
      "day": "27.09",
      "names": [
        "Dagmar",
        "Dagrun"
      ]
    },
    {
      "day": "28.09",
      "names": [
        "Lena",
        "Lene"
      ]
    },
    {
      "day": "29.09",
      "names": [
        "Mikael",
        "Mikal",
        "Mikkel"
      ]
    },
    {
      "day": "30.09",
      "names": [
        "Helga",
        "Helge",
        "Hege"
      ]
    },
    {
      "day": "01.10",
      "names": [
        "Rebekka",
        "Remi"
      ]
    },
    {
      "day": "02.10",
      "names": [
        "Live",
        "Liv"
      ]
    },
    {
      "day": "03.10",
      "names": [
        "Evald",
        "Evelyn"
      ]
    },
    {
      "day": "04.10",
      "names": [
        "Frans",
        "Frank"
      ]
    },
    {
      "day": "05.10",
      "names": [
        "Brynjar",
        "Boye",
        "Bo"
      ]
    },
    {
      "day": "06.10",
      "names": [
        "Målfrid",
        "Møyfrid"
      ]
    },
    {
      "day": "07.10",
      "names": [
        "Birgitte",
        "Birgit",
        "Berit"
      ]
    },
    {
      "day": "08.10",
      "names": [
        "Benedikte",
        "Bente"
      ]
    },
    {
      "day": "09.10",
      "names": [
        "Leidulf",
        "Leif"
      ]
    },
    {
      "day": "10.10",
      "names": [
        "Fridtjof",
        "Frida",
        "Frits"
      ]
    },
    {
      "day": "11.10",
      "names": [
        "Kevin",
        "Kennet",
        "Kent"
      ]
    },
    {
      "day": "12.10",
      "names": [
        "Valter",
        "Vibeke"
      ]
    },
    {
      "day": "13.10",
      "names": [
        "Torgeir",
        "Terje",
        "Tarjei"
      ]
    },
    {
      "day": "14.10",
      "names": [
        "Kaia",
        "Kai"
      ]
    },
    {
      "day": "15.10",
      "names": [
        "Hedvig",
        "Hedda"
      ]
    },
    {
      "day": "16.10",
      "names": [
        "Flemming",
        "Finn"
      ]
    },
    {
      "day": "17.10",
      "names": [
        "Marta",
        "Marte"
      ]
    },
    {
      "day": "18.10",
      "names": [
        "Kjersti",
        "Kjerstin"
      ]
    },
    {
      "day": "19.10",
      "names": [
        "Tora",
        "Tore"
      ]
    },
    {
      "day": "20.10",
      "names": [
        "Henrik",
        "Heine",
        "Henrikke"
      ]
    },
    {
      "day": "21.10",
      "names": [
        "Bergljot",
        "Birger"
      ]
    },
    {
      "day": "22.10",
      "names": [
        "Karianne",
        "Karine",
        "Kine"
      ]
    },
    {
      "day": "23.10",
      "names": [
        "Severin",
        "Søren"
      ]
    },
    {
      "day": "24.10",
      "names": [
        "Eilif",
        "Eivor"
      ]
    },
    {
      "day": "25.10",
      "names": [
        "Margrete",
        "Merete",
        "Märtha"
      ]
    },
    {
      "day": "26.10",
      "names": [
        "Amandus",
        "Amanda"
      ]
    },
    {
      "day": "27.10",
      "names": [
        "Sturla",
        "Sture"
      ]
    },
    {
      "day": "28.10",
      "names": [
        "Simon",
        "Simen"
      ]
    },
    {
      "day": "29.10",
      "names": [
        "Noralf",
        "Norunn"
      ]
    },
    {
      "day": "30.10",
      "names": [
        "Aksel",
        "Åmund",
        "Ove"
      ]
    },
    {
      "day": "31.10",
      "names": [
        "Edit",
        "Edna"
      ]
    },
    {
      "day": "01.11",
      "names": [
        "Veslemøy",
        "Vetle"
      ]
    },
    {
      "day": "02.11",
      "names": [
        "Tove",
        "Tuva"
      ]
    },
    {
      "day": "03.11",
      "names": [
        "Raymond",
        "Roy"
      ]
    },
    {
      "day": "04.11",
      "names": [
        "Otto",
        "Ottar"
      ]
    },
    {
      "day": "05.11",
      "names": [
        "Egil",
        "Egon"
      ]
    },
    {
      "day": "06.11",
      "names": [
        "Leonard",
        "Lennart"
      ]
    },
    {
      "day": "07.11",
      "names": [
        "Ingebrigt",
        "Ingelin"
      ]
    },
    {
      "day": "08.11",
      "names": [
        "Ingvild",
        "Yngvild"
      ]
    },
    {
      "day": "09.11",
      "names": [
        "Tordis",
        "Teodor"
      ]
    },
    {
      "day": "10.11",
      "names": [
        "Gudbjørg",
        "Gudveig"
      ]
    },
    {
      "day": "11.11",
      "names": [
        "Martin",
        "Morten",
        "Martine"
      ]
    },
    {
      "day": "12.11",
      "names": [
        "Torkjell",
        "Torkil"
      ]
    },
    {
      "day": "13.11",
      "names": [
        "Kirsten",
        "Kirsti"
      ]
    },
    {
      "day": "14.11",
      "names": [
        "Fredrik",
        "Fred",
        "Freddy"
      ]
    },
    {
      "day": "15.11",
      "names": [
        "Oddfrid",
        "Oddvar"
      ]
    },
    {
      "day": "16.11",
      "names": [
        "Edmund",
        "Edgar"
      ]
    },
    {
      "day": "17.11",
      "names": [
        "Hugo",
        "Hogne",
        "Hauk"
      ]
    },
    {
      "day": "18.11",
      "names": [
        "Magne",
        "Magny"
      ]
    },
    {
      "day": "19.11",
      "names": [
        "Elisabet",
        "Lisbet"
      ]
    },
    {
      "day": "20.11",
      "names": [
        "Halvdan",
        "Helle"
      ]
    },
    {
      "day": "21.11",
      "names": [
        "Mariann",
        "Marianne"
      ]
    },
    {
      "day": "22.11",
      "names": [
        "Cecilie",
        "Silje",
        "Sissel"
      ]
    },
    {
      "day": "23.11",
      "names": [
        "Klement",
        "Klaus"
      ]
    },
    {
      "day": "24.11",
      "names": [
        "Gudrun",
        "Guro"
      ]
    },
    {
      "day": "25.11",
      "names": [
        "Katarina",
        "Katrine",
        "Kari"
      ]
    },
    {
      "day": "26.11",
      "names": [
        "Konrad",
        "Kurt"
      ]
    },
    {
      "day": "27.11",
      "names": [
        "Torlaug",
        "Torleif"
      ]
    },
    {
      "day": "28.11",
      "names": [
        "Ruben",
        "Rut"
      ]
    },
    {
      "day": "29.11",
      "names": [
        "Sofie",
        "Sonja"
      ]
    },
    {
      "day": "30.11",
      "names": [
        "Andreas",
        "Anders"
      ]
    },
    {
      "day": "01.12",
      "names": [
        "Arnold",
        "Arnljot",
        "Arnt"
      ]
    },
    {
      "day": "02.12",
      "names": [
        "Borghild",
        "Borgny",
        "Bård"
      ]
    },
    {
      "day": "03.12",
      "names": [
        "Sveinung",
        "Svein"
      ]
    },
    {
      "day": "04.12",
      "names": [
        "Barbara",
        "Barbro"
      ]
    },
    {
      "day": "05.12",
      "names": [
        "Stine",
        "Ståle"
      ]
    },
    {
      "day": "06.12",
      "names": [
        "Nils",
        "Nikolai"
      ]
    },
    {
      "day": "07.12",
      "names": [
        "Hallfrid",
        "Hallstein"
      ]
    },
    {
      "day": "08.12",
      "names": [
        "Marlene",
        "Marion",
        "Morgan"
      ]
    },
    {
      "day": "09.12",
      "names": [
        "Anniken",
        "Annette"
      ]
    },
    {
      "day": "10.12",
      "names": [
        "Judit",
        "og",
        "Jytte"
      ]
    },
    {
      "day": "11.12",
      "names": [
        "Daniel",
        "Dan"
      ]
    },
    {
      "day": "12.12",
      "names": [
        "Pia",
        "Peggy"
      ]
    },
    {
      "day": "13.12",
      "names": [
        "Lucia",
        "Lydia"
      ]
    },
    {
      "day": "14.12",
      "names": [
        "Steinar",
        "Stein"
      ]
    },
    {
      "day": "15.12",
      "names": [
        "Hilda",
        "Hilde"
      ]
    },
    {
      "day": "16.12",
      "names": [
        "Oddbjørg",
        "Oddbjørn"
      ]
    },
    {
      "day": "17.12",
      "names": [
        "Inga",
        "Inge"
      ]
    },
    {
      "day": "18.12",
      "names": [
        "Kristoffer",
        "Kate"
      ]
    },
    {
      "day": "19.12",
      "names": [
        "Iselin",
        "Isak"
      ]
    },
    {
      "day": "20.12",
      "names": [
        "Abraham",
        "Amund"
      ]
    },
    {
      "day": "21.12",
      "names": [
        "Tomas",
        "Tom",
        "Tommy"
      ]
    },
    {
      "day": "22.12",
      "names": [
        "Ingemar",
        "Ingar"
      ]
    },
    {
      "day": "23.12",
      "names": [
        "Sigurd",
        "Sjur"
      ]
    },
    {
      "day": "24.12",
      "names": [
        "Adam",
        "Eva"
      ]
    },
    {
      "day": "25.12",
      "names": [
        "Ingen",
        "navnedag"
      ]
    },
    {
      "day": "26.12",
      "names": [
        "Stefan",
        "Steffen"
      ]
    },
    {
      "day": "27.12",
      "names": [
        "Narve",
        "Natalie"
      ]
    },
    {
      "day": "28.12",
      "names": [
        "Unni",
        "Une",
        "Unn"
      ]
    },
    {
      "day": "29.12",
      "names": [
        "Vidar",
        "Vemund"
      ]
    },
    {
      "day": "30.12",
      "names": [
        "David",
        "Diana",
        "Dina"
      ]
    },
    {
      "day": "31.12",
      "names": [
        "Sylfest",
        "Sylvia",
        "Sylvi"
      ]
    }
  ];

export default namedays;
